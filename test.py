from app.timetable.functions import generate_timetable_for_user
from app.users.models import User
import pprint
pp = pprint.PrettyPrinter(indent=4)
user = User.get()
tt = generate_timetable_for_user(user)
pp.pprint(tt)
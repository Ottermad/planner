from app.models import db

from app.school.models import School
from app.users.models import User, Role, UserRoles
from app.subjects.models import Subject, TeacherSubjects
from app.sets.models import Set, StudentSets, TeacherSets
from app.homework.models import Homework, UserHomeworks
from app.timetable.models import Period, Week, Timetable

db.create_tables([
    School,
    User,
    Role,
    UserRoles,
    Subject,
    TeacherSubjects,
    Set,
    StudentSets,
    TeacherSets,
    Homework,
    UserHomeworks,
    Period,
    Week,
    Timetable,
], safe=True)

from flask_wtf import (
    Form,
)

from wtforms import (
    StringField,
    PasswordField,
    SelectField,
    BooleanField,
)

from wtforms.validators import (
    DataRequired,
    ValidationError,
    Length,
    EqualTo,
)

from .models import School


def school_exists(form, field):
    if School.select().where(School.school_name == field.data).exists():
        raise ValidationError("School already in use.")


class SignUpForm(Form):
    school_name = StringField("School Name", validators=[DataRequired(), school_exists])
    forename = StringField("Forename", validators=[DataRequired()])
    surname = StringField("Surname", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired(), Length(min=5), EqualTo("password2", message="Passwords must match.")])
    password2 = PasswordField("Confirm Password", validators=[DataRequired()])


class NewUserForm(Form):
    forename = StringField("Forename", validators=[DataRequired()])
    surname = StringField("Surname", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired(), Length(min=5), EqualTo("password2", message="Passwords must match.")])
    password2 = PasswordField("Confirm Password", validators=[DataRequired()])
    is_admin = BooleanField("Is Admin")
    is_teacher = BooleanField("Is Teacher")
    is_student = BooleanField("Is Student")


class ModifyUserForm(Form):
    forename = StringField("Forename", validators=[DataRequired()])
    surname = StringField("Surname", validators=[DataRequired()])
    is_admin = BooleanField("Is Admin")
    is_teacher = BooleanField("Is Teacher")
    is_student = BooleanField("Is Student")
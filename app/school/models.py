from app.models import db, generate_table_name
from peewee import *

class School(Model):
    school_id = PrimaryKeyField(db_column="ID")
    school_name = CharField(max_length=255, unique=True, db_column="SCHOOL_NAME")

    class Meta:
        database = db
        db_table = "TBL_SCHOOL"

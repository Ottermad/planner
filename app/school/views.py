from flask import Blueprint, render_template, redirect, url_for, g, flash, request
from flask.ext.login import login_required
from .models import School
from .forms import SignUpForm, NewUserForm, ModifyUserForm
from .functions import create_school, get_users_in_school, get_role_names
from app.users.decorators import admin_required, teacher_required, student_required
from app.users.functions import create_user, add_roles_to_user, is_admin, is_student, is_teacher
from app.users.models import Role, User, UserRoles

school_blueprint = Blueprint("school_blueprint", __name__)


@school_blueprint.route("/signup", methods=("POST", "GET"))
def signup():
    form = SignUpForm()
    if form.validate_on_submit():
        created_successfully = create_school(
            school_name=form.school_name.data,
            admin_forename=form.forename.data,
            admin_surname=form.surname.data,
            admin_password=form.password.data
        )
        if created_successfully:
            return redirect(url_for(".home"))
    return render_template("school/signup.html", form=form)


@school_blueprint.route("/home")
@login_required
def home():
    name = g.user.forename + " " + g.user.surname
    user_roles = UserRoles.select().where(UserRoles.user_id == g.user.user_id)
    roles = [Role.get(Role.role_id == user_role.role_id).role_name for user_role in user_roles]
    return render_template("school/home.html", name=name, roles=roles)


@school_blueprint.route("/user-listing")
@login_required
@admin_required
def user_listing():
    school = School.get(School.school_id == g.user.school_id)
    users = get_users_in_school(school)
    roles = {}
    for user in users:
        roles[user.user_id] = ",".join(get_role_names(user))

    return render_template("school/user_listing.html", users=users, roles=roles)


@school_blueprint.route("/add-user", methods=("POST", "GET"))
@login_required
@admin_required
def add_user():
    form = NewUserForm()
    if form.validate_on_submit():
        username = form.forename.data[0] + form.surname.data
        username = username.lower()
        created_successfully = create_user(
            school=g.user.school_id,
            forename=form.forename.data,
            surname=form.surname.data,
            username=username,
            password=form.password.data
        )
        if not created_successfully[0]:
            flash(created_successfully[1])
        else:
            roles = []

            if form.is_admin.data:
                admin = Role.get(
                    Role.school_id == g.user.school_id,
                    Role.role_name == "admin"
                )
                roles.append(admin)

            if form.is_teacher.data:
                teacher = Role.get(
                    Role.school_id == g.user.school_id,
                    Role.role_name == "teacher"
                )
                roles.append(teacher)

            if form.is_student.data:
                student = Role.get(
                    Role.school_id == g.user.school_id,
                    Role.role_name == "student"
                )
                roles.append(student)
            add_roles_to_user(created_successfully[2], roles)

        if True:
            return redirect(url_for(".user_listing"))

    return render_template("school/add_user.html", form=form)


@school_blueprint.route("/modify-user/<id>", methods=("POST", "GET"))
@login_required
@admin_required
def modify_user(id):
    # Fetch user
    try:
        user = User.get(User.user_id == id)
        role_names = get_role_names(user)
    except:
        flash("Error fetching user. Perhaps it does not exist.")
        return redirect(url_for(".add_user"))

    # Check user belongs to g.user's school
    if not (user.school_id == g.user.school_id):
        flash("You do not have permissions to access that user.")
        return redirect(url_for(".add_user"))

    # Load form with user values
    user_data = {
        "forename": user.forename,
        "surname": user.surname,
        "is_admin": "admin" in role_names,
        "is_teacher": "teacher" in role_names,
        "is_student": "student" in role_names
    }

    if request.method == "GET":
        form = ModifyUserForm(**user_data)
    else:
        form = ModifyUserForm()

    if form.validate_on_submit():
        user.forename = form.forename.data
        user.surname = form.surname.data

        roles = []

        if form.is_admin.data:
            admin = Role.get(
                Role.school_id == g.user.school_id,
                Role.role_name == "admin"
            )
            roles.append(admin)
        elif is_admin(user):
            admin = Role.get(
                Role.school_id == g.user.school_id,
                Role.role_name == "admin"
            )
            UserRoles.get(UserRoles.user_id == user.user_id, UserRoles.role_id == admin.role_id).delete_instance()

        if form.is_teacher.data:
            teacher = Role.get(
                Role.school_id == g.user.school_id,
                Role.role_name == "teacher"
            )
            roles.append(teacher)
        elif is_teacher(user):
            teacher = Role.get(
                Role.school_id == g.user.school_id,
                Role.role_name == "teacher"
            )
            UserRoles.get(UserRoles.user_id == user.user_id, UserRoles.role_id == teacher.role_id).delete_instance()

        if form.is_student.data:
            student = Role.get(
                Role.school_id == g.user.school_id,
                Role.role_name == "student"
            )
            roles.append(student)
        elif is_student(user):
            student = Role.get(
                Role.school_id == g.user.school_id,
                Role.role_name == "student"
            )
            UserRoles.get(UserRoles.user_id == user.user_id, UserRoles.role_id == student.role_id).delete_instance()

        add_roles_to_user(user, roles)
        return redirect(url_for(".user_listing"))

    return render_template("school/modify_user.html", form=form)

from .models import School
from app.users.models import Role, User, UserRoles
from app.users.functions import create_user, add_roles_to_user, is_teacher, is_student


def create_school(school_name, admin_forename, admin_surname, admin_password):
    """Create a School object and related objects

    :param school_name: the unique name of the school
    :type school_name: str
    :param admin_forename: the forename for the default admin user
    :type admin_forename: str
    :param admin_surname: the surname for the default admin user
    :type admin_surname: str
    :param admin_password: the password for the default admin user
    :type admin_password: str
    :returns: either an error message or success message
    :rtype tuple
    """

    # Create the school
    try:
        school = School.create(
            school_name=school_name
        )
    except:
        return (False, "Error attempting to create school.")

    # Create roles for the school
    roles = ["admin", "teacher", "student"]
    for role in roles:
        try:
            role_object = Role.create(
                role_name=role,
                school_id=school
            )
        except:
            return (False, "Error attempting to create {}".format(role))

    role_objects = [Role.get(role_name="admin", school_id=school)]

    # Create admin user for the school
    admin_username = admin_forename[0] + admin_surname
    admin_username = admin_username.lower()

    created_user_successfully = create_user(
        school=school,
        forename=admin_forename,
        surname=admin_surname,
        username=admin_username,
        password=admin_password
    )

    if not created_user_successfully[0]:
        return created_user_successfully

    user = created_user_successfully[2]

    # Add roles to user
    added_roles = add_roles_to_user(user, role_objects)
    return added_roles


def get_users_in_school(school):
    users = []
    print("SCHOOL ID" + str(school.school_id))
    for user in User.select().where(User.school_id == school.school_id):
        users.append(user)
    return users


def get_teachers_in_school(school):
    users = []
    print("SCHOOL ID" + str(school.school_id))
    for user in User.select().where(User.school_id == school.school_id):
        if is_teacher(user):
            users.append(user)
    return users


def get_students_in_school(school):
    users = []
    print("SCHOOL ID" + str(school.school_id))
    for user in User.select().where(User.school_id == school.school_id):
        if is_student(user):
            users.append(user)
    return users


def get_role_names(user):
    role_ids = [user_role.role_id for user_role in UserRoles.select().where(UserRoles.user_id == user.user_id)]
    role_names = [role.role_name for role in Role.select().where(Role.role_id << role_ids)]
    return role_names

from flask_wtf import Form

from wtforms import (
    SelectField,
    TextAreaField,
)

from wtforms.fields.html5 import DateField

from wtforms.validators import (
    DataRequired,
)


class NewHomeworkForm(Form):
    assigned_set = SelectField("Set", validators=[DataRequired()], coerce=int)
    description = TextAreaField("Description", validators=[DataRequired()])
    date_due = DateField("Date Due", validators=[DataRequired()])


class ModifyHomeworkForm(Form):
    description = TextAreaField("Description", validators=[DataRequired()])
    date_due = DateField("Date Due", validators=[DataRequired()])

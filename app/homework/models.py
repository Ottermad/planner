from app.models import db
from app.sets.models import Set
from app.users.models import User
from peewee import *


class Homework(Model):
    homework_id = PrimaryKeyField(db_column="ID")
    set_id = ForeignKeyField(Set, db_column="SET_ID")
    description = CharField(max_length=255, db_column="DESCRIPTION")
    date_due = DateField(db_column="DATE_DUE")
    teacher_id = ForeignKeyField(User, db_column="TEACHER_ID")

    class Meta:
        database = db
        db_table = "TBL_HOMEWORK"


class UserHomeworks(Model):
    user_homework_id = PrimaryKeyField(db_column="ID")
    user_id = ForeignKeyField(User, db_column="USER_ID")
    homework_id = ForeignKeyField(Homework, db_column="HOMEWORK_ID")

    class Meta:
        database = db
        db_table = "TBL_USER_HOMEWORKS"

from flask import Blueprint, render_template, g, request, \
    flash, redirect, url_for
from flask.ext.login import login_required
from peewee import DoesNotExist

from app.users.decorators import teacher_required, student_required

from .forms import NewHomeworkForm, ModifyHomeworkForm
from .functions import add_homework_for_set, get_homework_for_set
from .models import Homework

from app.sets.functions import get_sets_from_teacher, get_sets_from_student
from app.sets.models import Set

from app.users.models import User

homework_blueprint = Blueprint("homework_blueprint", __name__)


@homework_blueprint.route("/add-homework", methods=("POST", "GET"))
@login_required
@teacher_required
def add_homework():
    form = NewHomeworkForm()

    # Get sets
    form.assigned_set.choices = [(choice.set_id, choice.set_name) for choice in get_sets_from_teacher(g.user)]

    if form.validate_on_submit():
        assigned_set = Set.get(Set.set_id == form.assigned_set.data)
        add_homework_for_set(
            set_to_assign_to=assigned_set,
            description=form.description.data,
            date_due=form.date_due.data,
            teacher=g.user
        )

    return render_template("homework/new_homework.html", form=form)


@homework_blueprint.route("/modify-homework/<id>", methods=("POST", "GET"))
@login_required
@teacher_required
def modify_homework(id):

    try:
        homework = Homework.get(Homework.homework_id == id)
    except DoesNotExist as e:
        # Flash error and redirect
        flash(e.message)
        return redirect(url_for(".teacher_homework_listing"))

    if request.method == "GET":
        # Preload
        form = ModifyHomeworkForm(data={
            "description": homework.description,
            "date_due": homework.date_due
        })
    else:
        form = ModifyHomeworkForm()

    if form.validate_on_submit():
        if form.description.data != homework.description:
            homework.description = form.description.data
            homework.save()

        if form.date_due.data != homework.date_due:
            homework.date_due = form.date_due.data
            homework.save()

    return render_template("homework/modify_homework.html", form=form)


@homework_blueprint.route("/teacher-homework-listing")
@login_required
@teacher_required
def teacher_homework_listing():
    teachers_sets = get_sets_from_teacher(g.user)
    data = []
    for teacher_set in teachers_sets:
        homework = get_homework_for_set(teacher_set)
        for item in homework:
            data.append([teacher_set.set_name, item.description, str(item.date_due)])

    return render_template("homework/teacher_homework_listing.html", homework=data)


@homework_blueprint.route("/student-homework-listing")
@login_required
@student_required
def student_homework_listing():
    students_sets = get_sets_from_student(g.user)
    print 1
    data = []
    print 2
    for student_set in students_sets:
        homework = get_homework_for_set(student_set)
        for item in homework:
            data.append([student_set.set_name, item.description, str(item.date_due)])

    return render_template("homework/student_homework_listing.html", homework=data)

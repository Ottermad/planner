from .models import Homework, UserHomeworks
from app.sets.models import Set
from app.sets.functions import get_current_students_for_set
from peewee import *
from datetime import date

def add_homework_for_set(set_to_assign_to, description, date_due, teacher):
    try:
        homework = Homework.create(
            set_id=set_to_assign_to.set_id,
            description=description,
            date_due=date_due,
            teacher_id=teacher.user_id
        )
    except IntegrityError as e:
        print e.message
        return (False, "Error creating homework")

    for student in get_current_students_for_set(set_to_assign_to):
        success = add_user_homework(student, homework)
        if not success[0]:
            return success[1]

    return (True, "")


def add_user_homework(user, homework):
    try:
        UserHomeworks.create(
            user_id=user.user_id,
            homework_id=homework.homework_id
        )
    except:
        return (False, "Error associating homework {} with user {}".format(
            homework.homework_id,
            user.user_id
        ))
    return (True, "")


def get_homework_for_set(set_to_search):
    query = Homework.select().where(
        (Homework.set_id == set_to_search.set_id) &
        (Homework.date_due >= date.today())
    )
    print "QUERIED", query
    homework = [item for item in query]
    return homework
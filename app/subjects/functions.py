from app.users.models import User
from .models import Subject, TeacherSubjects


def create_subject(name, school, teachers):
    """
    :param name: Name of the subject
    :type name: str
    :param school: School for the subject to belong to
    :type school: School
    :param teachers: Teachers to teach the subject
    :type teachers: list of User
    """
    # Create subject
    try:
        subject = Subject.create(
            school_id=school.school_id,
            subject_name=name
        )
    except:
        return (False, "Error attempting to create subject {} for school {}",
                name, school.school_name)

    add_teachers_to_subject(teachers, subject)
    return (True, "")


def add_teachers_to_subject(teachers, subject):
    """
    :param teachers: Teachers to add the subject
    :type teachers: list of User objects
    :param subject: The subject to add the teachers to
    :type subject: Subject object
    """
    # Create TeacherSubject objects for each User object in teachers list
    for teacher in teachers:
        try:
            TeacherSubjects.create(
                subject_id=subject.subject_id,
                teacher_id=teacher.user_id
            )
        except:
            return (False, "Error attempting to create TeacherSubjects for "
                    "teacher with id {} and subject with id {}".format(
                        subject.subject_id, teacher.user_id))
    return (True, "")


def get_subjects_and_teachers(school):
    subjects = Subject.select().where(
        Subject.school_id == school.school_id)
    data = {}
    for subject in subjects:
        data[subject] = get_teachers_for_subject(subject)
    return data


def get_teachers_for_subject(subject):
    teacher_subjects = TeacherSubjects.select().where(
        TeacherSubjects.subject_id == subject.subject_id)

    users = [User.get(User.user_id == teacher_subject.teacher_id)
             for teacher_subject in teacher_subjects]
    return users


def get_subjects_for_school(school):
    subjects = Subject.select().where(
        Subject.school_id == school.school_id)
    data = [subject for subject in subjects]
    return data

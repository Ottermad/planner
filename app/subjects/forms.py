from flask_wtf import Form

from wtforms import (
    StringField,
    SelectMultipleField,
)

from wtforms.validators import (
    DataRequired,
    ValidationError,
)

from app.subjects.models import Subject


def subject_name_exists(form, field):
    if Subject.select().where(Subject.subject_name == field.data).exists():
        raise ValidationError("Name already in use")


class NewSubjectForm(Form):
    name = StringField("Subject Name", validators=[DataRequired()])
    teachers = SelectMultipleField("Teachers", coerce=int)


class ModifySubjectForm(Form):
    name = StringField("Subject Name", validators=[DataRequired()])
    remove_teachers = SelectMultipleField("Remove Teachers", coerce=int)
    add_teachers = SelectMultipleField("Add Teachers", coerce=int)

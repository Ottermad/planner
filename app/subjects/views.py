from flask import Blueprint, render_template, g, redirect, url_for, request
from flask.ext.login import login_required
from .forms import NewSubjectForm, ModifySubjectForm
from .functions import (
    create_subject, get_subjects_and_teachers,
    get_teachers_for_subject, add_teachers_to_subject)
from .models import Subject, TeacherSubjects

from app.school.models import School
from app.school.functions import get_teachers_in_school

from app.users.models import User
from app.users.decorators import admin_required

subjects_blueprint = Blueprint("subjects_blueprint", __name__)


@subjects_blueprint.route("/add-subject", methods=("POST", "GET"))
@login_required
@admin_required
def add_subject():
    form = NewSubjectForm()
    school = School.get(School.school_id == g.user.school_id)

    teachers = get_teachers_in_school(school)
    teacher_choices = [(teacher.user_id, teacher.forename + " " +
                        teacher.surname) for teacher in teachers]
    form.teachers.choices = teacher_choices

    if form.validate_on_submit():
        print form.teachers.data
        ids = form.teachers.data

        teachers = [teacher for teacher in User.select().where(
            User.user_id << ids)]

        create_subject(
            name=form.name.data,
            school=school,
            teachers=teachers)
        return redirect(url_for("school_blueprint.home"))

    return render_template("subjects/add_subject.html", form=form)


@subjects_blueprint.route("/subject-listing")
@login_required
@admin_required
def subject_listing():
    school = School.get(School.school_id == g.user.school_id)
    data = get_subjects_and_teachers(school)
    string_data = {}
    for key in data.keys():
        string_data[key.subject_name] = []
        users = data[key]
        for user in users:
            string_data[key.subject_name].append(
                user.forename + " " + user.surname)
    return render_template("subjects/subject_listing.html",
                           subjects=string_data)


@subjects_blueprint.route("/modify-subject/<id>", methods=("POST", "GET"))
@login_required
@admin_required
def modify_subject(id):
    # Get the subject being modified
    subject = Subject.get(Subject.subject_id == id)

    # Check if the form needs the name to be added
    if request.method == "GET":
        # Set forms name
        form = ModifySubjectForm(data={"name": subject.subject_name})
    else:
        # Get the form
        form = ModifySubjectForm()

    # Get teachers already teaching the subject
    remove_teachers = get_teachers_for_subject(subject)

    # Get current user's school
    school = School.get(School.school_id == g.user.school_id)

    # Get all the teachers in school
    teachers = get_teachers_in_school(school)

    # Remove teachers already teaching the subject
    for teacher in remove_teachers:
        teachers.pop(teachers.index(teacher))

    # Set choices to the id and names of the teachers
    form.remove_teachers.choices = [
        (teacher.user_id, teacher.forename + " " + teacher.surname)
        for teacher in remove_teachers]
    form.add_teachers.choices = [
        (teacher.user_id, teacher.forename + " " + teacher.surname)
        for teacher in teachers]

    # Check if the form is validate
    if form.validate_on_submit():
        if subject.subject_name != form.name.data:
            subject.subject_name = form.name.data

        teachers_to_add = []
        for user_id in form.add_teachers.data:
            user = User.get(User.user_id == user_id)
            teachers_to_add.append(user)
        add_teachers_to_subject(teachers_to_add, subject)

        remove_query = TeacherSubjects.delete().where(
            (
                TeacherSubjects.subject_id == subject.subject_id
            ) & (
                TeacherSubjects.teacher_id << form.remove_teachers.data
            )
        )
        remove_query.execute()
        return redirect(url_for(".subject_listing"))
    return render_template("subjects/add_subject.html", form=form)

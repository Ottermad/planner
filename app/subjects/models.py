from app.models import db, generate_table_name
from app.school.models import School
from app.users.models import User
from peewee import *

class Subject(Model):
    subject_id = PrimaryKeyField(db_column="ID")
    school_id = ForeignKeyField(School, db_column="SCHOOL_ID")
    subject_name = CharField(db_column="SUBJECT_NAME")

    def __str__(self):
        return str({
            "subject_id": self.subject_id,
            "school_id": self.school_id.school_id,
            "subject_name": str(self.subject_name)
        })

    class Meta:
        database = db
        db_table = "TBL_SUBJECT"


class TeacherSubjects(Model):
    teacher_subject_id = PrimaryKeyField(db_column="ID")
    subject_id = ForeignKeyField(Subject, db_column="SUBJECT_ID")
    teacher_id = ForeignKeyField(User, db_column="TEACHER_ID")

    class Meta:
        database = db
        db_table = "TBL_TEACHER_SUBJECTS"

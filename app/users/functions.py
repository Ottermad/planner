from .models import User,UserRoles, Role
from flask.ext.bcrypt import generate_password_hash
from flask import flash


def create_user(school, forename, surname, username, password):
    """Create a user with a hashed password

    :param school: School that user will be associated with
    :type school: School
    :param forename: forename for the user
    :type forename: str
    :param surname: surname for the user
    :type surname: str
    :param username: username for the user
    :type username: str
    :param password: unhashed password for the user
    :type password: str
    :return: tuple
    """
    try:
        user = User.create(
            school_id=school,
            forename=forename,
            surname=surname,
            username=username,
            password=generate_password_hash(password)
        )
        return (True, "", user)
    except:
        return (False, "Error creating User {}".format(username))


def add_roles_to_user(user, roles):
    for role in roles:
        try:
            UserRoles.create(
                user_id=user,
                role_id=role
            )
        except:
            return (False, "Error adding {} role to {} user".format(role, user))
    return (True, "")


def is_admin(user):
    # Get role
    try:
        admin = Role.get(
            Role.school_id == user.school_id,
            Role.role_name == "admin"
        )
    except:
        return False

    if UserRoles.select().where((UserRoles.user_id == user.user_id) & (UserRoles.role_id == admin)).exists():
        return True
    return False


def is_teacher(user):
    # Get role
    try:
        admin = Role.get(
            Role.school_id == user.school_id,
            Role.role_name == "teacher"
        )
    except:
        return False

    if UserRoles.select().where((UserRoles.user_id == user.user_id) & (UserRoles.role_id == admin)).exists():
        return True
    return False


def is_student(user):
    # Get role
    try:
        admin = Role.get(
            Role.school_id == user.school_id,
            Role.role_name == "student"
        )
    except:
        return False

    if UserRoles.select().where((UserRoles.user_id == user.user_id) & (UserRoles.role_id == admin)).exists():
        return True
    return False

from flask import Blueprint, redirect, url_for, render_template, flash
from .forms import LoginForm
from .models import User
from peewee import DoesNotExist
from flask.ext.bcrypt import check_password_hash
from flask.ext.login import login_user, current_user, logout_user

users_blueprint = Blueprint("users_blueprint", __name__)


@users_blueprint.route("/login", methods=("POST", "GET"))
def login():
    form = LoginForm()
    print(current_user)
    if form.validate_on_submit():
        try:
            user = User.get(User.username == form.username.data)
        except DoesNotExist:
            flash("Your email or password does not exist.")
        else:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                flash("You have been logged in.")
                return redirect(url_for("school_blueprint.home"))
            else:
                flash("Your email or password does not exist.")
    return render_template("users/login.html", form=form)


@users_blueprint.route("/logout")
def logout():
    logout_user()
    flash("You have been logged out!")
    return redirect(url_for(".login"))
from app.models import db, generate_table_name
from app.school.models import School
from peewee import *
from flask.ext.login import UserMixin


class User(UserMixin, Model):
    user_id = PrimaryKeyField(db_column="ID")
    school_id = ForeignKeyField(School, db_column="SCHOOL_ID")
    forename = CharField(db_column="FORENAME")
    surname = CharField(db_column="SURNAME")
    username = CharField(unique=True, db_column="USERNAME")
    password = CharField(db_column="PASSWORD")

    class Meta:
        database = db
        db_table = "TBL_USER"

    def get_id(self):
        """Return user_id for flask-login"""
        return self.user_id


class Role(Model):
    role_id = PrimaryKeyField(db_column="ID")
    role_name = CharField(unique=True, db_column="ROLE_NAME")
    school_id = ForeignKeyField(School, db_column="SCHOOL_ID")

    class Meta:
        database = db
        db_table = "TBL_ROLE"


class UserRoles(Model):
    user_roles_id = PrimaryKeyField(db_column="ID")
    user_id = ForeignKeyField(User, db_column="USER_ID")
    role_id = ForeignKeyField(Role, db_column="ROLE_ID")

    class Meta:
        database = db
        db_table = "TBL_USER_ROLES"

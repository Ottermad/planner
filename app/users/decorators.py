from flask import redirect, url_for, request, g, flash
from functools import wraps
from .functions import is_admin, is_teacher, is_student


def admin_required(view):
    @wraps(view)
    def decorated_function(*args, **kwargs):
        if not is_admin(g.user):
            flash("You need to be an admin to access that page!")
            return redirect(url_for("school_blueprint.home"))
        return view(*args, **kwargs)
    return decorated_function


def teacher_required(view):
    @wraps(view)
    def decorated_function(*args, **kwargs):
        if not is_teacher(g.user):
            flash("You need to be a teacher to access that page!")
            return redirect(url_for("school_blueprint.home"))
        return view(*args, **kwargs)
    return decorated_function


def student_required(view):
    @wraps(view)
    def decorated_function(*args, **kwargs):
        if not is_student(g.user):
            flash("You need to be a student to access that page!")
            return redirect(url_for("school_blueprint.home"))
        return view(*args, **kwargs)
    return decorated_function


def teacher_or_student_required(view):
    @wraps(view)
    def decorated_function(*args, **kwargs):
        if not is_student(g.user) and not is_teacher(g.user):
            flash("You need to be a student or teacher to access that page!")
            return redirect(url_for("school_blueprint.home"))
        return view(*args, **kwargs)
    return decorated_function

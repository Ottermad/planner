from peewee import *
import os
import urlparse

if 'LOCAL_DEV' in os.environ:
    db = SqliteDatabase('planner.db')
else:
    urlparse.uses_netloc.append('postgres')
    url = urlparse.urlparse(os.environ['DATABASE_URL'])

    database = {
        'engine': 'peewee.PostgresqlDatabase',
        'name': url.path[1:],
        'user': url.username,
        'password': url.password,
        'host': url.hostname,
        'port': url.port,
    }
    db = PostgresqlDatabase(database["name"],user=database["user"],password=database["password"],host=database["host"],port=database["port"])


def generate_table_name(model):
    name = model.__name__
    tbl_name = "TBL"
    for char in name:
        if char.isupper():
            tbl_name += "_"
        tbl_name += char.upper()
    return tbl_name

from playhouse.migrate import *
from app.models import db

migrator = SqliteMigrator(db)

migrate(
    migrator.rename_column("TBL_HOMEWORK", "description", "DESCRIPTION"),
    migrator.rename_column("TBL_HOMEWORK", "date_due", "DATE_DUE"),
    migrator.rename_column("TBL_HOMEWORK", "teacher_id_id", "TEACHER_ID")
)

from playhouse.migrate import *
from app.models import db
from app.timetable.models import Timetable

migrator = SqliteMigrator(db)

migrate(
    migrator.add_column("TBL_TIMETABLE", "DAY", Timetable.day)
)

from .models import Period, Week, Timetable, Day
from app.sets.models import Set
from app.sets.functions import get_current_students_for_set, get_current_teachers_for_set
from peewee import *
from collections import OrderedDict


def create_period(school, name, start, end):
    try:
        Period.create(
            school_id=school,
            period_name=name,
            start_time=start,
            end_time=end
        )
        return True, ""
    except:
        return False, "Error creating Period called {} for School {}".format(
            name, school.school_name
        )


def create_week(name, school):
    try:
        Week.create(
            week_name=name,
            school_id=school
        )
        return True, ""
    except:
        return False, "Error creating Week called {} for School {}".format(
            name, school.school_name
        )


def get_periods_for_school(school):
    periods = []
    for period in Period.select().where(Period.school_id == school):
        periods.append(period)
    return periods


def create_timetable_for_set(period, week, set_object, day):
    for person in get_current_students_for_set(set_object) + get_current_teachers_for_set(set_object):
        try:
            Timetable.create(
                period=period,
                week=week,
                user=person,
                set=set_object,
                day=day
            )
        except:
            return False, "Error creating Timetable with period {}, week {}, student {}, set {}".format(
                period.period_name,
                week.week_name,
                person.user_id,
                set_object.set_name)

    return True, ""


def generate_timetable_for_user(user):
    """

    :param user: User
    :return:
    """
    """timetable = {
        "Week A": [
            "Period"
        ]
    }"""
    timetable = OrderedDict()
    for week in Week.select().where(Week.school_id == user.school_id):
        timetable[str(week)] = OrderedDict()
        for day in Day.select():
            timetable[str(week)][str(day)] = OrderedDict()
            for period in Period.select().where(Period.school_id == user.school_id):
                try:
                    tt = Timetable.get(
                        Timetable.period == period,
                        Timetable.week == week,
                        Timetable.day == day,
                        Timetable.user == user.user_id
                    )
                    set_object = Set.get(Set.set_id == tt.set)
                except DoesNotExist:
                    set_object = None
                timetable[str(week)][str(day)][str(period)] = str(set_object)
    return timetable
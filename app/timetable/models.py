from peewee import *

from app.models import db
from app.school.models import School
from app.users.models import User
from app.sets.models import Set

"""
class Period(Model):
    period_id = PrimaryKeyField(db_column="ID")
    school_id = ForeignKeyField(School, db_column="SCHOOL_ID")
    period_name = CharField(db_column="PERIOD_NAME")
    start_time = TimeField(db_column="START_TIME")
    end_time = TimeField(db_column="END_TIME")

    def __str__(self):
        return self.period_name

    class Meta:
        database = db
        db_table = "TBL_PERIOD"


class Week(Model):
    week_id = PrimaryKeyField(db_column="ID")
    school_id = ForeignKeyField(School, db_column="SCHOOL_ID")
    week_name = CharField(db_column="WEEK_NAME")

    def __str__(self):
        return self.week_name

    class Meta:
        database = db
        db_table = "TBL_WEEK"


class Day(Model):
    day_id = PrimaryKeyField(db_column="ID")
    name = CharField(db_column="NAME")

    def __str__(self):
        return self.name

    class Meta:
        database = db
        db_table = "TBL_DAY"


class Timetable(Model):
    timetable_id = PrimaryKeyField(db_column="ID")
    period = ForeignKeyField(Period, db_column="PERIOD_ID")
    week = ForeignKeyField(Week, db_column="WEEK_ID")
    user = ForeignKeyField(User, db_column="USER_ID")
    set = ForeignKeyField(Set, db_column="SET_ID")
    day = ForeignKeyField(Day, db_column="DAY_ID")

    class Meta:
        database = db
        db_table = "TBL_TIMETABLE"
"""


class Week(Model):
    week_id = PrimaryKeyField(db_column="ID")
    school_id = ForeignKeyField(School, db_column="SCHOOL_ID")
    week_name = CharField(db_column="WEEK_NAME")

    def __str__(self):
        return str({
            "week_id": self.week_id,
            "school_id": self.school_id.school_id,
            "week_name": str(self.week_name)
        })

    class Meta:
        database = db
        db_table = "TBL_WEEK"


class Period(Model):
    period_id = PrimaryKeyField(db_column="ID")
    week = ForeignKeyField(Week, db_column="WEEK_ID")
    day = IntegerField(db_column="DAY")  # Monday - 1 Friday - 5
    start_time = TimeField()
    end_time = TimeField()
    name = CharField()

    def __str__(self):
        return str({
            "period_id": self.period_id,
            "week": self.week.week_id,
            "day": self.day,
            "start_time": str(self.start_time),
            "end_time": str(self.end_time),
            "name": str(self.name)
        })

    class Meta:
        database = db
        db_table = "TBL_PERIOD"


class Timetable(Model):
    id = PrimaryKeyField(db_column="ID")
    period = ForeignKeyField(Period, db_column="PERIOD_ID")
    set = ForeignKeyField(Set, db_column="SET_ID")
    user = ForeignKeyField(User, db_column="USER_ID")

    def __dict__(self):
        return {
            "id": self.id,
            "period": self.period,
            "set": self.set,
            "user": self.user
        }

    class Meta:
        database = db
        db_table = "TBL_TIMETABLE"

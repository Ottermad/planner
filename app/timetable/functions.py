from .models import Period, Week, Timetable
from app.sets.models import Set
from app.sets.functions import get_current_students_for_set, get_current_teachers_for_set
from peewee import *
from collections import OrderedDict


def create_week(name, school):
    try:
        Week.create(
            week_name=name,
            school_id=school
        )
        return True, ""
    except:
        return False, "Error creating Week called {} for School {}".format(
            name, school.school_name
        )


def get_weeks_for_school(school):
    weeks = []
    for week in Week.select().where(Week.school_id == school):
        weeks.append(week)
    return weeks


def day_num_to_str(num):
    days = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
    ]
    return days[num - 1]


def day_str_to_num(str):
    days = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
    ]
    return days.index(str) + 1


def create_period(week, day, start, end, name):
    try:
        Period.create(
            week=week,
            day=day,
            name=name,
            start_time=start,
            end_time=end
        )
        return True, ""
    except IntegrityError as e:
        print e.message
        return False, "Error creating Period called {} on day {} in week {} starting at {} and ending at {}".format(
            name, day_num_to_str(day), week.week_name, start, end
        )


def get_periods_for_school(school):
    periods = []
    for period in Period.select().join(Week).where(Period.week == Week.week_id, Week.school_id == school.school_id):
        periods.append(period)
    return periods


def create_timetable_for_set(period, set_object):
    for person in get_current_students_for_set(set_object) + get_current_teachers_for_set(set_object):
        try:
            Timetable.create(
                period=period,
                user=person,
                set=set_object,
            )
        except:
            return False, "Error creating Timetable with period {}, student {}, set {}".format(
                period.period_name,
                person.user_id,
                set_object.set_name)

    return True, ""


def get_timetable_for_user(user):
    """
    {
        Week: {
            "period 1": {
                "Set": {}
                {}
            }
        }

    }
    """
    data = OrderedDict()
    for tt in Timetable.select().join(Period).where(Timetable.user == user.user_id).order_by(Period.week, Period.day):
        week_in = False
        f_key = None
        for key in data.keys():
            if key.week_id == tt.period.week.week_id:
                week_in = True
                f_key = key

        if week_in is False:
            f_key = tt.period.week
            data[tt.period.week] = OrderedDict()

        data[f_key][tt.period] = {
            "set": tt.set,
            "subject": tt.set.subject_id
        }
    return data

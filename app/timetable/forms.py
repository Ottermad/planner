from flask_wtf import (
    Form,
)

from wtforms import (
    StringField,
    SelectMultipleField,
)

from wtforms_components import TimeField

from wtforms.validators import (
    DataRequired,
)


class NewWeekForm(Form):
    name = StringField("Week Name", validators=[DataRequired()])


class NewPeriodForm(Form):
    name = StringField("Period Name", validators=[DataRequired()])
    week = SelectMultipleField("Week", validators=[DataRequired()], coerce=int)
    day = SelectMultipleField("Day", validators=[DataRequired()],
                              choices=[(1, "Monday"),
                                       (2, "Tuesday"),
                                       (3, "Wednesday"),
                                       (4, "Thursday"),
                                       (5, "Friday")],
                              coerce=int)
    start_time = TimeField("Start Time", validators=[DataRequired()])
    end_time = TimeField("End Time", validators=[DataRequired()])


class NewTimetableForm(Form):
    set = SelectMultipleField("Set", validators=[DataRequired()], coerce=int)
    period = SelectMultipleField("Period", validators=[DataRequired()], coerce=int)

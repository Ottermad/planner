from flask import Blueprint, render_template, g, flash, jsonify
from flask.ext.login import login_required
from app.users.decorators import admin_required, teacher_or_student_required
from .forms import NewPeriodForm, NewWeekForm, NewTimetableForm
from .functions import create_period, create_week, get_periods_for_school, \
    get_weeks_for_school, day_num_to_str, create_timetable_for_set, get_timetable_for_user
from .models import Week, Period
from app.sets.models import Set
from app.sets.functions import get_sets_from_school
from app.school.models import School
import json
timetable_blueprint = Blueprint("timetable_blueprint", __name__)


@timetable_blueprint.route("/add-period", methods=("POST","GET"))
@login_required
@admin_required
def add_period():
    form = NewPeriodForm()

    weeks = [[week.week_id, week.week_name] for week in  get_weeks_for_school(g.user.school_id)]
    form.week.choices = weeks

    if form.validate_on_submit():
        print type(form.start_time.data)
        for week_id in form.week.data:
            for day in form.day.data:
                success = create_period(
                    week=Week.get(Week.week_id == week_id),
                    day=day,
                    start=form.start_time.data,
                    end=form.end_time.data,
                    name=form.name.data
                )
                if not success[0]:
                    flash(success[1])

    return render_template("timetable/add_period.html", form=form)


@timetable_blueprint.route("/add-week", methods=("POST", "GET"))
@login_required
@admin_required
def add_week():
    form = NewWeekForm()

    if form.validate_on_submit():
        created_successfully = create_week(
            name=form.name.data,
            school=g.user.school_id,
        )

        if created_successfully[0]:
            # TODO: Add redirect
            pass

        flash(created_successfully[1])

    return render_template("timetable/add_week.html", form=form)


@timetable_blueprint.route("/week-period-listing")
@login_required
@admin_required
def week_period_listing():
    periods = [
        [
            period.period_id,
            period.name,
            period.week.week_name,
            day_num_to_str(period.day),
            str(period.start_time),
            str(period.end_time),
        ]
        for period in get_periods_for_school(g.user.school_id)
    ]
    weeks = [
        [week.week_id, week.week_name] for week in get_weeks_for_school(
            School.get(School.school_id == g.user.school_id)
        )
    ]
    return render_template("timetable/week_period_listing.html", periods=periods, weeks=weeks)


@timetable_blueprint.route("/add-timetable", methods=("POST", "GET"))
@login_required
@admin_required
def add_timetable():
    form = NewTimetableForm()

    form.set.choices = [(set_object.set_id, set_object.set_name) for set_object in get_sets_from_school(g.user.school_id)]
    form.period.choices = [
        (
            period.period_id,
            "Period: " + period.name + " Week: " + period.week.week_name + " Day: " + day_num_to_str(period.day)
        )
        for period in get_periods_for_school(g.user.school_id)
    ]

    if form.validate_on_submit():
        for set_id in form.set.data:
            for period_id in form.period.data:
                created_successfully = create_timetable_for_set(
                    period=Period.get(Period.period_id == period_id),
                    set_object=Set.get(Set.set_id == set_id)
                )
            if not created_successfully[0]:
                break

            flash(created_successfully[1])

    return render_template("timetable/add_timetable.html", form=form)


@timetable_blueprint.route("/timetable")
@login_required
@teacher_or_student_required
def timetable():
    import pprint
    pp = pprint.PrettyPrinter()
    user_timetable = get_timetable_for_user(g.user)
    data = {}
    for week in user_timetable.keys():
        data[week.week_name] = {}
        for period in user_timetable[week].keys():
            """if period.day not in data[week.week_name].keys():
                data[week.week_name][day_num_to_str(period.day)] = {}

            if period.period_id not in data[week.week_name][day_num_to_str(period.day)].keys():
                data[week.week_name][day_num_to_str(period.day)][period.name] = {}"""
            if period.name not in data[week.week_name].keys():
                data[week.week_name][period.name] = []

            data[week.week_name][period.name].append(user_timetable[week][period]["subject"].subject_name)
    print data
    return render_template("timetable/timetable.html", timetable=data)

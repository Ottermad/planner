import os
from flask import Flask, abort, request, jsonify, g, url_for, Blueprint
from flask.ext.httpauth import HTTPBasicAuth
from flask.ext.bcrypt import check_password_hash
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

from app import app
from app.users.models import User
from app.users.decorators import student_required, teacher_or_student_required

from app.homework.functions import get_homework_for_set

from app.sets.functions import get_sets_from_student
from app.sets.models import Set

from collections import OrderedDict

from app.timetable.functions import get_timetable_for_user
from app.timetable.models import Timetable, Period

from app.timetable.functions import get_timetable_for_user

import json

api_blueprint = Blueprint("api_blueprint", __name__, url_prefix="/api")

auth = HTTPBasicAuth()


def generate_auth_token(user, expiration=600):
    s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
    return s.dumps({'id': user.user_id})


def verify_auth_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    print data
    try:
        user = User.get(User.user_id == data['id'])
        return user
    except:
        return None


@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        try:
            user = User.get(User.username == username_or_token)
        except:
            return False
        if not user or not check_password_hash(user.password, password):
            return False
    g.user = user
    return True


@api_blueprint.route('/token')
@auth.login_required
def get_auth_token():
    token = generate_auth_token(g.user, 600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@api_blueprint.route('/homework')
@auth.login_required
@student_required
def get_resource():
    students_sets = get_sets_from_student(g.user)
    data = []
    for student_set in students_sets:
        homework = get_homework_for_set(student_set)
        for item in homework:
            teacher = User.get(User.user_id == item.teacher_id)
            data.append({
                "id": item.homework_id,
                "set_id": item.set_id.set_id,
                "set_name": Set.get(Set.set_id == item.set_id).set_name,
                "description": item.description,
                "date_due": str(item.date_due),
                "teacher": teacher.forename + " " + teacher.surname
            })

    return jsonify({'homework': data})


@api_blueprint.route("/timetable")
@auth.login_required
@teacher_or_student_required
def timetable():
    """ut = get_timetable_for_user(g.user)
    print ut
    data = OrderedDict()
    for key in ut.keys():
        data[str(key)] = OrderedDict()
        for key2 in ut[key].keys():
            data[str(key)][str(key2)] = {
                "set": str(ut[key][key2]["set"]),
                "subject": str(ut[key][key2]["subject"])
            }
    return jsonify(data)"""
    ut = get_timetable_for_user(g.user)
    data = OrderedDict()
    for key in ut.keys():
        data[key.week_name] = OrderedDict()
        for key2 in ut[key].keys():
            if key2.day not in data[key.week_name]:
                data[key.week_name][key2.day] = OrderedDict()

            data[key.week_name][key2.day][key2.name] = {
                "set_name": ut[key][key2]["set"].set_name,
                "subject": ut[key][key2]["subject"].subject_name,
            }
    print data
    return jsonify(data)

def tt_dict(tt):
    return {
        "id": tt.id,
        "period": tt.period,
        "set": tt.set,
        "user": tt.user
    }

def week_dict(week):
    return {
        "week_id": week.week_id,
        "school_id": week.school_id,
        "week_name": week.week_name
    }

def period_dict(period):
    return {
        "period_id": period.period_id,
        "week": period.week,
        "day": period.day,
        "start_time": period.start_time,
        "end_time": period.end_time,
        "name": period.name
    }

def set_dict(set_o):
    return {
        "set_id": set_o.set_id,
        "subject_id": set_o.subject_id,
        "set_name": set_o.set_name
    }

def subject_dict(subject):
    return {
        "subject_id": subject.subject_id,
        "school_id": subject.school_id,
        "subject_name": subject.subject_name
    }
from flask import *
from flask.ext.login import LoginManager, current_user
from app.models import db
from app.users.models import User
from app.users.functions import is_student, is_teacher, is_admin
from app.timetable.functions import day_num_to_str

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = "nkafcneqnfdwadad2i12nmkwdawadwad"
app.jinja_env.globals.update(
    is_teacher=is_teacher,
    is_admin=is_admin,
    is_student=is_student,
    day_str=day_num_to_str
)

# Set up login manager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "users_blueprint.login"


@login_manager.user_loader
def load_user(userid):
    try:
        return User.get(User.user_id == userid)
    except:
        return None


@app.before_request
def before_request():
    """Connect to database before each request"""
    g.db = db
    g.db.connect()
    g.user = current_user


@app.after_request
def after_request(response):
    """Close the database connection after each response"""
    g.db.close()
    return response


@app.route("/")
def index():
    return render_template("index.html")

from app.school.views import school_blueprint
app.register_blueprint(school_blueprint)

from app.users.views import users_blueprint
app.register_blueprint(users_blueprint)

from app.subjects.views import subjects_blueprint
app.register_blueprint(subjects_blueprint)

from app.sets.views import sets_blueprint
app.register_blueprint(sets_blueprint)

from app.homework.views import homework_blueprint
app.register_blueprint(homework_blueprint)

from app.timetable.views import timetable_blueprint
app.register_blueprint(timetable_blueprint)

from app.api import api_blueprint
app.register_blueprint(api_blueprint)
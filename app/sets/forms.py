from flask_wtf import (
    Form,
)

from wtforms import (
    StringField,
    SelectMultipleField,
)

from wtforms.validators import (
    DataRequired,
)


class NewSetForm(Form):
    name = StringField("Name")
    teachers = SelectMultipleField("Teachers", coerce=int)
    students = SelectMultipleField("Students", coerce=int)


class ModifySetForm(Form):
    name = StringField("Name", validators=[DataRequired()])
    current_teachers = SelectMultipleField("Current Teachers (select to remove)", coerce=int)
    new_teachers = SelectMultipleField("Add Teachers", coerce=int)
    current_students = SelectMultipleField("Current Students (select to remove)", coerce=int)
    new_students = SelectMultipleField("Add Students", coerce=int)


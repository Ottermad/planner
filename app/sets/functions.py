from .models import Set, TeacherSets, StudentSets
from app.subjects.models import TeacherSubjects, Subject
from app.subjects.functions import get_subjects_for_school

from app.users.models import User

def create_set(name, subject, teachers, students):
    """
    :param name: the name of set to be created
    :type name: str
    :param subject: the subject of the set
    :type subject: Subject
    :param teachers: the teachers who will teach the set
    :type teachers: list User
    :param students: the students in the set
    :type students: list User
    """

    try:
        set = Set.create(
            subject_id=subject.subject_id,
            set_name=name)
    except:
        return (False, "Error creating set.")

    success = add_teachers_to_set(set, teachers)
    if not success[0]:
        return success
    success = add_students_to_set(set, students)
    if not success[0]:
        return success
    return (True, "")


def add_students_to_set(set_to_add_to, students):
    for student in students:
        try:
            StudentSets.create(
                set_id=set_to_add_to.set_id,
                user_id=student.user_id)
        except:
            return (False, "Error creating StudentSets {} for {} {}".format(
                set_to_add_to.set_name, student.forename, student.surname))
    return (True, "")


def add_teachers_to_set(set_to_add_to, teachers):
    subject = Subject.get(Subject.subject_id == set_to_add_to.subject_id)
    for teacher in teachers:
        teaches_subject = TeacherSubjects.select().where(
            (TeacherSubjects.subject_id == subject.subject_id)
            &
            (TeacherSubjects.teacher_id == teacher.user_id)
        ).exists()
        if teaches_subject:
            try:
                TeacherSets.create(
                    set_id=set_to_add_to.set_id,
                    teacher_id=teacher.user_id)
            except:
                return (False,
                        "Error creating TeacherSets {} for {} {}".format(
                            set_to_add_to.set_name,
                            teacher.forename, teacher.surname))
        else:
            return (False, "Teacher {} {} does not teach {}".format(
                teacher.forename, teacher.surname, subject.subject_name))
    return (True, "")


def get_sets_and_teachers_from_school(school):
    subjects = get_subjects_for_school(school)
    sets = {}
    for subject in subjects:
            query = Set.select().where(Set.subject_id == subject.subject_id)
            for set_object in query:
                teacher_sets = TeacherSets.select().where(
                    TeacherSets.set_id == set_object.set_id)
                teachers = []
                for teacher_set in teacher_sets:
                    teachers.append(User.get(
                        User.user_id == teacher_set.teacher_id))
                sets[set_object] = teachers
    return sets


def get_sets_from_school(school):
    subjects = get_subjects_for_school(school)
    sets = []
    for subject in subjects:
            query = Set.select().where(Set.subject_id == subject.subject_id)
            for set_object in query:
                sets.append(set_object)
    return sets


def get_current_teachers_for_set(set_to_modify):
    teacher_sets = TeacherSets.select().where(TeacherSets.set_id == set_to_modify.set_id)
    current_teachers = []
    for teacher_set in teacher_sets:
        teacher = User.get(User.user_id == teacher_set.teacher_id)
        current_teachers.append(teacher)
    return current_teachers


def get_current_students_for_set(set_to_fetch_from):
    student_sets = StudentSets.select().where(StudentSets.set_id == set_to_fetch_from.set_id)
    current_students = []
    for student_set in student_sets:
        student = User.get(User.user_id == student_set.user_id)
        current_students.append(student)
    return current_students


def get_sets_from_teacher(teacher):
    teacher_sets = TeacherSets.select().where(TeacherSets.teacher_id == teacher.user_id)
    sets_to_return = []
    for teacher_set in teacher_sets:
        sets_to_return.append(Set.get(Set.set_id == teacher_set.set_id))
    return sets_to_return


def get_sets_from_student(student):
    student_sets = StudentSets.select().where(StudentSets.user_id == student.user_id)
    sets_to_return = []
    for student_set in student_sets:
        sets_to_return.append(Set.get(Set.set_id == student_set.set_id))
    return sets_to_return

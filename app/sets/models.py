from app.models import db, generate_table_name
from app.subjects.models import Subject
from app.users.models import User
from peewee import *


class Set(Model):
    set_id = PrimaryKeyField(db_column="ID")
    subject_id = ForeignKeyField(Subject, db_column="SUBJECT_ID")
    set_name = CharField()

    def __str__(self):
        return str({
            "set_id": self.set_id,
            "subject_id": self.subject_id.subject_id,
            "set_name": str(self.set_name)
        })

    class Meta:
        database = db
        db_table = "TBL_SET"


class StudentSets(Model):
    student_set_id = PrimaryKeyField(db_column="ID")
    set_id = ForeignKeyField(Set, db_column="SET_ID")
    user_id = ForeignKeyField(User, db_column="USER_ID")

    class Meta:
        database = db
        db_table = "TBL_STUDENT_SETS"


class TeacherSets(Model):
    teacher_set_id = PrimaryKeyField(db_column="ID")
    set_id = ForeignKeyField(Set, db_column="SET_ID")
    teacher_id = ForeignKeyField(User, db_column="TEACHER_ID")

    class Meta:
        database = db
        db_table = "TBL_TEACHER_SETS"
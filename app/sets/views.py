from flask import Blueprint, g, render_template, redirect, url_for, flash, request
from flask.ext.login import login_required

from app.users.decorators import admin_required, teacher_required, student_required

from .forms import NewSetForm, ModifySetForm
from .functions import create_set, get_sets_and_teachers_from_school, get_current_students_for_set, \
    get_current_teachers_for_set, add_students_to_set, add_teachers_to_set, \
    get_sets_from_teacher, get_sets_from_student
from .models import Set, TeacherSets, StudentSets

from app.subjects.functions import get_teachers_for_subject
from app.subjects.models import Subject

from app.school.models import School
from app.school.functions import get_students_in_school

from app.users.models import User

sets_blueprint = Blueprint("sets_blueprint", __name__)


@sets_blueprint.route("/add-set/<subject>", methods=("POST", "GET"))
@login_required
@admin_required
def add_set(subject):
    form = NewSetForm()
    school = School.get(School.school_id == g.user.school_id)

    # Get subject
    subject = Subject.get(Subject.subject_id == subject,
                          Subject.school_id == school.school_id)

    # Get teachers
    teachers = [(teacher.user_id, teacher.forename + " " + teacher.surname)
                for teacher in get_teachers_for_subject(subject)]
    # Get students
    students = [(student.user_id, student.forename + " " + student.surname)
                for student in get_students_in_school(school)]

    form.teachers.choices = teachers
    form.students.choices = students

    if form.validate_on_submit():

        teachers_in_set = []
        for teacher_id in form.teachers.data:
            teachers_in_set.append(User.get(User.user_id == teacher_id))

        students_in_set = []
        for student_id in form.students.data:
            students_in_set.append(User.get(User.user_id == student_id))

        create_set(
            name=form.name.data,
            subject=subject,
            teachers=teachers_in_set,
            students=students_in_set)

        return redirect(url_for(".set_listing"))

    return render_template("sets/sets_form.html", form=form)


@sets_blueprint.route("/set-listing")
@login_required
@admin_required
def set_listing():
    school = School.get(School.school_id == g.user.school_id)
    sets_and_teachers = get_sets_and_teachers_from_school(school)
    string_sets_and_teachers = {}
    for key in sets_and_teachers.keys():
        string_sets_and_teachers[key.set_name] = [
            item.forename + " " + item.surname for item in sets_and_teachers[key]]

    return render_template("sets/set_listing.html", sets_and_teachers=string_sets_and_teachers)


@sets_blueprint.route("/teacher-set-listing")
@login_required
@teacher_required
def teacher_set_listing():
    sets = get_sets_from_teacher(g.user)
    data = []
    for item in sets:
        data.append(item.set_name)
    return render_template("sets/teacher_set_listing.html", sets=data)


@sets_blueprint.route("/student-set-listing")
@login_required
@student_required
def student_set_listing():
    sets = get_sets_from_student(g.user)
    data = []
    for item in sets:
        data.append(item.set_name)
    return render_template("sets/student_set_listing.html", sets=data)


@sets_blueprint.route("/modify-set/<id>", methods=("POST", "GET"))
@login_required
@admin_required
def modify_set(id):

    # Get set based on id param
    # TODO: Restrict to sets from same school as admin
    try:
        set_to_modify = Set.get(Set.set_id == id)
    except:
        flash("Invalid set id")
        return redirect(".set_listing")

    # Check whether form needs to be prepopulated with a name
    if request.method == "GET":
        form = ModifySetForm(data={"name": set_to_modify.set_name}) # Create form and set name value for it
    else:
        form = ModifySetForm()

    # Get current teachers
    current_teachers = get_current_teachers_for_set(set_to_modify)
    current_teachers_choices = [(teacher.user_id, teacher.forename + " " + teacher.surname) for teacher in
                                current_teachers]

    # Get current students
    current_students = get_current_students_for_set(set_to_modify)
    current_students_choices = [(student.user_id, student.forename + " " + student.surname) for student in
                                current_students]

    # Get subject
    subject = Subject.get(Subject.subject_id == set_to_modify.subject_id)

    # Get school
    school = School.get(School.school_id == subject.school_id)

    # Get teachers
    subject_teachers = get_teachers_for_subject(subject)
    print subject_teachers
    for teacher in current_teachers:
        subject_teachers.remove(teacher)
    teachers_choices = [(teacher.user_id, teacher.forename + " " + teacher.surname) for teacher in
                        subject_teachers]

    # Get students
    students = get_students_in_school(school)

    for student in current_students:
        students.remove(student)
    students_choices = [(student.user_id, student.forename + " " + student.surname) for student in
                        students]

    # Assign choices
    form.current_students.choices = current_students_choices
    form.current_teachers.choices = current_teachers_choices
    form.new_students.choices = students_choices
    form.new_teachers.choices = teachers_choices

    if form.validate_on_submit():
        # Check if set name has changed and if so updated
        if form.name.data == set_to_modify.set_name:
            set_to_modify.set_name = form.name.data
            set_to_modify.save()

        # Remove teachers selected
        query = TeacherSets.delete().where(TeacherSets.teacher_id << form.current_teachers.data)
        query.execute()

        # Remove students selected
        query = StudentSets.delete().where(StudentSets.user_id << form.current_students.data)
        query.execute()

        # Add new teachers
        teachers_to_add = []
        for teacher in subject_teachers:
            if teacher.user_id in form.new_teachers.data:
                teachers_to_add.append(teacher)
        add_teachers_to_set(set_to_modify, teachers_to_add)

        # Add new students
        students_to_add = []
        for student in students:
            if student.user_id in form.new_students.data:
                students_to_add.append(student)
        add_students_to_set(set_to_modify, students_to_add)

        return redirect(url_for(".set_listing"))

    return render_template("sets/sets_form.html", form=form)


